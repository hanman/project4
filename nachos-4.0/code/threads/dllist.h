class DLLElement {
public:
	DLLElement(void* itemPtr, int sortKey) {
		key = sortKey;
		item = itemPtr;
	}
	DLLElement *next;
	DLLElement *prev;
	int key;
	void *item;
};


class DLList {
public:
	DLList() {
		first = NULL;
		last = NULL;
	}
	/*˜DLList(){
	while(!IsEmpty){
	Remove(&first->key);
	}
	}*/
	void Prepend(void *item);
	void Append(void *item);
	void *Remove(int *keyPtr);
	bool IsEmpty();
	void SortedInsert(void *item, int sortKey);
	void *SortedRemove(int sortKey);


private:
	DLLElement *first;
	DLLElement *last;
};
void insert_random(int, DLList *);
void delete_random(int, DLList *);
